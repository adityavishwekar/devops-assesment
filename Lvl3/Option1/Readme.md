Steps to create Jenkins pipeline

1. Login into Jenkins dashboard and Click the New Item menu with Jenkins

2. In the Enter an item name field, specify the name for your new Pipeline project.

3. Scroll down and click Pipeline, then click OK at the end of the page. ( Optional ) On the next page, specify a brief description for your Pipeline in the Description field 

4. Click the Pipeline tab at the top of the page to scroll down to the Pipeline section.

5. From the Definition field, choose the Pipeline script from SCM option. This option instructs Jenkins to obtain your Pipeline from Source Control Management (SCM), which will be your locally cloned Git repository.

6. From the SCM field, choose Git and provide URL and all the details.

7. Click Save to save your new Pipeline project. Now, you can go ahead and Edit Jenkinsfile

8. Download the sample Jenkinsfile from this repository. Edit steps for 'Checkout' stage and provide git url, branch and credential information and push it to your repository.

9. Go back to your pipeline project and click 'Build'
